const puppeteer = require('puppeteer');
const assert = require('assert');

require('dotenv').config()

const url = 'https://fizz.ca/en';

(async () => {

    function missingParameter(paramName) {
        return `Mandatory parameter ${paramName} is not defined.`;
    }

    const browser = await puppeteer.launch({
        headless: false,
        // slowMo: 100 // slow down by 250ms
    });
    const page = await browser.newPage();
    
    assert(url, missingParameter('process.env.PM_URL'));
    assert(process.env.PM_USER, missingParameter('process.env.PM_USER'));
    assert(process.env.PM_PASS, missingParameter('process.env.PM_PASS'));

    await page.goto(url);
    await page.click('a[href="/en/modal_login"]');
    await page.waitForSelector('input#email');
    await page.type('input#email', process.env.PM_USER);
    await page.type('input#password', process.env.PM_PASS);
    await page.click('button#submit-button-link');
    await page.waitForNavigation();
    await page.waitForSelector('app-plan-recommendation');
    await page.click('button.mb-md-3');
    await page.click('button.mb-md-3');
    await page.waitForNavigation();
    await page.waitForSelector('h3');
    const used = await page.$eval('h3', el => el.innerText);
    const days = await page.$eval('span.text-left', el => el.innerText);
    
    // debug: 
    // await page.screenshot({
    //     path: 'screenshot.png'
    // });

    console.info(`data: ${used} / time: ${days}`);

    browser.close();
})();