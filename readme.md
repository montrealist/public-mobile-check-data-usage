# Check your Public Mobile data usage

Tiny script that logs in to Public Mobile dashboard and prints the current mobile data usage to the console.

## Setup

You need to have an `.env` file with your username and password in the root directory of your project. Add environment-specific variables on new lines in the form of NAME=VALUE. Replace the XXX values in the below and paste it into the `.env` file:

```
PM_USER=xxx@xxxc.com
PM_PASS=XXX
PM_URL=https://selfserve.publicmobile.ca/
```

## How to run

```
npm i
npm start
```

Wait a few seconds, and you should see something like this in the console:

```
used / total: 42.42 / 1000 MB
```